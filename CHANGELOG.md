# Changelog
All notable changes to this project will be documented in this file.

The format is based on
[Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to
[Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 0.1.0 - 2018-09-15
### Added
- Protobuf definitions
- Lighter dispatcher
- Support for c-lightning node
- Support for eclair node
- Support for lnd node
- Unified error handling
- Runtime settings management
- Utils common module
- Configuration support
- Graceful exit and signal handling
- Logging
- Test suite
- Building, running, testing and linting orchestration via Makefile
- Docker support with autogeneration of Dockerfile and compose file
- arm32v7 support
- Documentation
